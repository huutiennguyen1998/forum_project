<?


session_start();
define('VALID_REQUIRE', true);

require_once 'db.php';
require_once 'connected.php';

if ($connected === false)
{
    header('Location: login.php');
    exit();
}



if (!isset($_GET['id']) || !is_numeric($_GET['id']))
    die('Invalid id');

$id = $_GET['id'];

$req = $db->prepare('SELECT * FROM topics WHERE tid = ?');
$req->execute(array($id));
if ($req->rowCount() == 0)
    die('Invalid id');
$topic = $req->fetch();
$req->closeCursor();


if (isset($_POST['answer']))
{
    $req = $db->prepare('INSERT INTO replies(user_id, topic_id, message) VALUE (:userId, :topicId, :message)');
    $req->bindParam(':userId', $connectedId);
    $req->bindParam(':topicId', $topic['tid']);
    $req->bindParam(':message', $_POST['answer']);
    $req->execute();
}


$req = $db->prepare('SELECT * FROM replies WHERE topic_id = ?');
$req->execute(array($topic['tid']));
// var_dump($req->fetch());


require_once 'header.php';
require_once 'navbar.php';
?>

<div class="container">
    <div class="row">
        <div class="col s12">
            <h3><?= $topic['t_subject'] ?></h3>
            <div><?= $topic['t_body']?></div>
        </div>
        <div class="col s12">
            <ul class="collection">
                <? while ($data = $req->fetch()):
                    $newReq = $db->prepare('SELECT * FROM users WHERE uid = :user_id');
                    $newReq->bindParam(':user_id', $data['user_id']);
                    $newReq->execute();
                    $creatorUsername = $newReq->fetch()['username'];
                    $newReq->closeCursor();
                    ?>
                    <li class="collection-item">
                        <div class="row">
                            <div class="col s4"><?= $creatorUsername ?></div>
                            <div class="col s8">
                                <div class="row">
                                    <div class="col s12"><?= $data['r_subject'] ?></div>
                                    <div class="col s12"><?= $data['r_body'] ?></div>
                                </div>
                            </div>
                        </div>
                    </li>
                <? endwhile; ?>
            </ul>
        </div>
        <form action="topic.php?id=<?= $topic['id'] ?>" method="post" class="col s6 offset-s3">
            <div class="row">
                <div class="input-field col s12">
                    <textarea class="materialize-textarea" id="answer-input" name="answer"></textarea>
                    <label for="answer-input">Message</label>
                </div>
                <div class="input-field col s12">
                    <input class="waves-effect waves-light btn" type="submit" value="Answer" />
                </div>
            </div>
        </form>
    </div>
</div>

<?
require_once 'footer.php';
$req->closeCursor();
?>