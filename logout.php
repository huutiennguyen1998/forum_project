<?

session_start();
session_destroy();

setcookie('username', '', 0);
setcookie('password', '', 0);

header('Location: login.php');
exit();
