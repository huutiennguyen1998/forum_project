<?
if (!defined('VALID_REQUIRE') || VALID_REQUIRE !== true)
    die('Invalid include');

try
{
    $db = new PDO('mysql:host=localhost;dbname=predator_forum;charset=utf8', 'root', 'root');
}
catch (Exception $exception)
{
    die('Failed to connect to the database.');
}
