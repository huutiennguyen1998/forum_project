<?

session_start();
define('VALID_REQUIRE', true);

require_once 'db.php';
require_once 'connected.php';

if ($connected === true)
{
    header('Location: index.php');
    exit();
}



// if (isset($_POST['username']) && isset($_POST['password']) && isset($_POST['stay_connected']))
if (isset($_POST['username']) && isset($_POST['password']))
{
    $username = $_POST['username'];
    $password = $_POST['password'];
    $stayConnected = $_POST['stay-connected'];

    $req = $db->prepare('SELECT * FROM users WHERE username = :username OR email = :email');
    $req->bindParam(':username', $username);
    $req->bindParam(':email', $username);
    $req->execute();

    while ($data = $req->fetch())
    {

        if (password_verify($password, $data['password']))
        {

            $req->closeCursor();
            header('Location: index.php');
            $_SESSION['username'] = $data['username'];
            $_SESSION['password'] = $data['password'];
            if ($stayConnected == true)
            {
                setcookie('username', $data['username'], time() + 3600 * 24 * 7, null, null, false, true);
                setcookie('password', $data['password'], time() + 3600 * 24 * 7, null, null, false, true);
            }
            exit();
        }
    }
    echo 'invalid username/email or password';
    $req->closeCursor();
}


require_once 'header.php';
require_once 'navbar.php';
?>

<div class="container">

    <div class="row">
        <form class="col s6 offset-s3" action="login.php" method="post">

            <div class="row">
                <div class="input-field col s12">
                    <input id="username-input" name="username" type="text" class="validate" />
                    <label for="username-input">Username or Email</label>
                </div>

                <div class="input-field col s12">
                    <input id="password-input" name="password" type="password" class="validate" />
                    <label for="password-input">Password</label>
                </div>

                <div class="input-field col s6">
                    <input class="waves-effect waves-light btn" type="submit" value="Connect" />
                </div>

                <div class="input-field col s6">
                    <label>
                        <input id="stay-connected-input" class="filled-in" name="stay-connected" type="checkbox" />
                        <span>Stay connected</span>
                    </label>
                </div>
            </div>
        </form>
    </div>

</div>

<?
require_once 'footer.php';
?>