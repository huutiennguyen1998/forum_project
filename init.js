(function ($) {
    $(function () {
        M.AutoInit();
        $('.datepicker').datepicker({
            format: 'dd/mm/yyyy',
            firstDay: 1,
            defaultDate: new Date(1990,01,01),
            yearRange: [1980,2000],
            i18n: {
                cancel: "Hủy",
                done: "Chọn",
                months: [
                    'Tháng 1',
                    'Tháng 2',
                    'Tháng 3',
                    'Tháng 4',
                    'Tháng 5',
                    'Tháng 6',
                    'Tháng 7',
                    'Tháng 8',
                    'Tháng 9',
                    'Tháng 10',
                    'Tháng 11',
                    'Tháng 12',
                ],
                monthsShort: [
                    'Tháng 1',
                    'Tháng 2',
                    'Tháng 3',
                    'Tháng 4',
                    'Tháng 5',
                    'Tháng 6',
                    'Tháng 7',
                    'Tháng 8',
                    'Tháng 9',
                    'Tháng 10',
                    'Tháng 11',
                    'Tháng 12',
                ],
                weekdaysShort: [
                    'CN',
                    'Thứ 2',
                    'Thứ 3',
                    'Thứ 4',
                    'Thứ 5',
                    'Thứ 6',
                    'Thứ 7',
                ],
                weekdaysAbbrev: ['CN','T2','T3','T4','T5','T6','T7']
            }
        });
        // $('.sidenav').sidenav();
        // $('.parallax').parallax();
        // $('.collapsible.expandable').collapsible({
        //     accordion: false,
        //     onOpenEnd: function(){
        //         // $('.collapsible-header')
        //         this.destroy();
        //         // $('ul > li:nth-child(1) > div.collapsible-header').click();
        //     }
        // });
        // $('.collapsible-header').on("click", function(){
        //     $('.collapsible-body').css('display', 'block');
        // });
        
    }); // end of document ready
})(jQuery); // end of jQuery name space