<?

session_start();
define('VALID_REQUIRE', true);

require_once 'db.php';
require_once 'connected.php';

if ($connected === true)
{
    header('Location: index.php');
    exit();
}



if (isset($_POST['username']) && isset($_POST['email']) && isset($_POST['password']))
{
    if (empty($_POST['username'] || $_POST['email'] || $_POST['password']))
    {
        echo "empty err";
        die();
    }
    $username = $_POST['username'];
    $password = $_POST['password'];
    $password_txt = $_POST['password'];
    $fullname = $_POST['fullname'];
    $avatar = $_POST['avatar'];
    $gender = $_POST['gender'];
    $birthday = $_POST['birthday'];
    $email = $_POST['email'];
    $location = $_POST['location'];

    $req = $db->prepare('SELECT * FROM users WHERE username = :username OR email = :email');
    $req->bindParam(':username', $username);
    $req->bindParam(':email', $email);
    $req->execute();

    if ($req->rowCount() > 0)
    {
        echo 'An account with theses information already exists.';
        header('Location: login.php');
        exit();
    }
    $password = password_hash($password, PASSWORD_BCRYPT);

    $req = $db->prepare('INSERT INTO users(username, password, password_txt, fullname, avatar, member_type, gender, birthday, email, location, registered_date, last_login) VALUE(:username, :password, :password_txt, :fullname, :avatar, 1, :gender, :birthday, :email, :location, NOW(), NOW())');
    $req->bindParam(':username', $username);
    $req->bindParam(':password', $password);
    $req->bindParam(':password_txt', $password_txt);
    $req->bindParam(':fullname', $fullname);
    $req->bindParam(':avatar', $avatar);
    $req->bindParam(':gender', $gender);
    $req->bindParam(':birthday', $birthday);
    $req->bindParam(':email', $email);
    $req->bindParam(':location', $location);
    $req->execute();

    echo "Successfully registered.";
    header('Location: login.php');
    exit();
}


require_once 'header.php';
require_once 'navbar.php';
?>

<div class="container">

    <div class="row">
        <form class="col s6 offset-s3" action="register.php" method="post">
            <div class="row">
                <div class="input-field col s12">
                    <input id="username-input" name="username" type="text" class="validate" />
                    <label for="username-input">Tên đăng nhập</label>
                </div>

                <div class="input-field col s12">
                    <input id="email-input" name="email" type="text" class="validate" />
                    <label for="email-input">Email</label>
                </div>

                <div class="input-field col s12">
                    <input id="password-input" name="password" type="password" class="validate" />
                    <label for="password-input">Mật khẩu</label>
                </div>

                <div class="input-field col s12">
                    <input id="password-check-input" name="password_check" type="password" class="validate" />
                    <label for="password-check-input">Nhập lại mật khẩu</label>
                </div>

                <div class="input-field col s12">
                    <input id="gender-input" name="gender" type="text" class="validate" />
                    <label for="gender-input">Giới tính</label>
                </div>

                <div class="input-field col s12">
                    <input id="birthday-input" name="birthday" type="text" class="validate datepicker" />
                    <label for="birthday-input">Ngày tháng năm sinh</label>
                </div>

                <div class="input-field col s12">
                    <i class="material-icons prefix">textsms</i>
                    <select id="location-select" name="location" class="validate">
                    <option value="Hồ Chí Minh" selected>Hồ Chí Minh</option>
                        <? include ("location_list.php");?>
                    </select>
                    <label for="location-select">Nơi ở</label>
                </div>

                <div class="input-field col s12">
                    <button class="waves-effect waves-light btn" type="submit">Đăng Ký
                    <i class="material-icons right">send</i>
                    </button>
                </div>
            </div>
        </form>
    </div>

</div>

<?
require_once 'footer.php';
?>

