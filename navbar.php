<?


if (!defined('VALID_REQUIRE') || VALID_REQUIRE !== true)
    die('Invalid include');

?>
<div class="navbar-fixed">
<nav class="nav-extended grey darken-4">
    <div class="container">
        <div class="nav-wrapper">
            <a href="index.php" class="brand-logo">PREDAT<img width="30px" class="responsive-img" src="icon.svg" alt="logo">R</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <? if ($connected === false): ?>
                    <li><a class="waves-effect waves-light modal-trigger" href="login.php"><i class="material-icons">perm_identity</i></a></li>
                    <li><a class="waves-effect waves-light modal-trigger" href="#modalRegister"><i class="material-icons">person_add</i></a></li>
                <? else: ?>
                    <li><a href="logout.php"><i class="material-icons">exit_to_app</i></a></li>
                <? endif; ?>
            </ul>
        </div>
    </div>
</nav>
</div>

<!-- breadcrumbs -->
<nav class="teal">
    <div class="container">
        <div class="nav-wrapper">
        <div class="col s12">
            <a href="index.php" class="breadcrumb waves-effect waves-teal"><i class="material-icons">home</i></a>
            <a href="javascript:void(0)" class="breadcrumb">Second</a>
            <a href="javascript:void(0)" class="breadcrumb">Third</a>
        </div>
        </div>
    </div>
</nav>
<!-- breadcrumbs -->

<!-- mobile menu left -->
<ul class="sidenav" id="mobile-demo">
    <li><div class="user-view">
      <div class="background">
        <img src="office.jpg">
      </div>
      <a href="#user"><img class="circle" src="yuna.jpg"></a>
      <a href="#name"><span class="white-text name">John Doe</span></a>
      <a href="#email"><span class="white-text email">jdandturk@gmail.com</span></a>
    </div></li>
    <li><a href="#!"><i class="material-icons">cloud</i>First Link With Icon</a></li>
    <li><a href="#!">Second Link</a></li>
    <li><div class="divider"></div></li>
    <li><a class="subheader">Subheader</a></li>
    <li><a class="waves-effect" href="#!">Third Link With Waves</a></li>
</ul>
<!-- mobile menu left -->

<!-- modals -->
<div id="modalLogin" class="modal">
    <div class="modal-content">
      <h4>Đăng Nhập</h4>
      <p>nội dung đăng nhập ở đây</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>
</div>
<div id="modalRegister" class="modal modal-fixed-footer">
    <div class="modal-content">
      <h4>Đăng Ký</h4>
      <p>Nội dung đăng ký ở đây</p>
    </div>
    <div class="modal-footer">
      <a href="#!" class="modal-close waves-effect waves-green btn-flat">Đóng</a>
    </div>
</div>
<!-- modals -->