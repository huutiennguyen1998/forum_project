<?

require_once 'db.php';
require_once 'connected.php';


function display_categories() {
    include ('db.php');
    include ('connected.php');
    
    // get categories
    $select = $db->query('SELECT * FROM categories');

    while ($row = $select->fetch()):
    ?>
        <ul class="collapsible expandable">
            <li class="active">
            <div class="collapsible-header"><i class="material-icons">forum</i><?=$row['category_name']?></div>
            <div class="collapsible-body white">
            <table class="striped">
                <tbody>

                    <? get_subcategories($row['cid']); ?>

                </tbody>
            </table>
            </div>
            </li>
        </ul>
    <?
    endwhile;
}

function get_subcategories($parent_id) {
    include ('db.php');
    include ('connected.php');
    
    // get sub categories
    $select = $db->query('SELECT * FROM subcategories WHERE parent_id = '.$parent_id);

    while ($row = $select->fetch()): ?>
        <tr>
            <td>
            <h3><?=$row['subcategory_name']?></h3>
            <br/>
            <?=$row['subcategory_desc']?>
            <br/>
            <? get_forums($row['scid']); ?>
            </td>
            <td><? get_last_topic($row['scid']); ?></td>
        </tr>
    <? endwhile;
}

function get_forums($subcat_id) {
    include ('db.php');
    include ('connected.php');
    
    // get forum name
    $select = $db->query('SELECT * FROM forums WHERE subcat_id = '.$subcat_id);

    while ($row = $select->fetch()): ?>
        <p><?=$row['forum_name']?></p>
    <? endwhile;
}

function get_last_topic($subcat_id) {
    include ('db.php');
    include ('connected.php');
    
    // get topics
    $select = $db->query('SELECT * FROM topics, forums WHERE forum_id = fid AND subcat_id = '.$subcat_id);

    while ($row = $select->fetch()): ?>
        <p><?=$row['topic_subject']?></p>
    <? endwhile;
}

?>